﻿using QuandParie.Core.Domain;
using QuandParie.Core.Services;
using QuandParie.Core.Persistance;
using System.Threading.Tasks;
using QuandParie.Core.ReadOnlyInterfaces;

namespace QuandParie.Core.Application
{
    public class RegistrationApplication
    {
        // TODO 2006-12-23: Aymeric essayes de mettre de meilleur noms
        // TODO 2007-02-04: Rah, c'est bon, c'est très bien comme ça, le code est concis
        // TODO 2012-05-18: Added documentation, but we need to complete them
        // TODO 2019-07-12: Find better names when I get some time

        /// <summary>
        /// c
        /// </summary>
        private readonly ICustomerRepository c;

        /// <summary>
        /// d
        /// </summary>
        private readonly IDocumentRepository d;

        /// <summary>
        /// p1
        /// </summary>
        private readonly IIdentityProofer p1;

        /// <summary>
        /// p2
        /// </summary>
        private readonly IAddressProofer p2;

        public RegistrationApplication(
            ICustomerRepository c, 
            IDocumentRepository d, 
            IIdentityProofer p1,
            IAddressProofer p2)
        {
            this.c = c;
            this.d = d;
            this.p1 = p1;
            this.p2 = p2;
        }

        /// <summary>
        /// Create the account
        /// </summary>
        /// <param name="s1">The s1</param>
        /// <param name="s2">The s2</param>
        /// <returns>The task of IReadOnlyCustomer</returns>
        public async Task<IReadOnlyCustomer> CreateAccount(string s1, string s2, string s3)
        {
            var o = new Customer(s1, s2, s3);
            await c.SaveAsync(o);

            return o;
        }

        /// <summary>
        /// Get the account
        /// </summary>
        /// <param name="s1">The s1</param>
        /// <returns>The task of IReadOnlyCustomer</returns>
        public async Task<IReadOnlyCustomer> GetAccount(string s)
        {
            return await c.GetAsync(s);
        }

        /// <summary>
        /// Upload the identity proof
        /// </summary>
        /// <param name="c2">The c2</param>
        /// <param name="b">The b</param>
        /// <returns>The task of bool</returns>
        public async Task<bool> UploadIdentityProof(string c2, byte[] b)
        {
            var c1 = await c.GetAsync(c2);
            if (!p1.Validates(c1, b))
                return false;

            await d.SaveAsync(DocumentType.IdentityProof, c2, b);

            c1.IsIdentityVerified = true;
            await c.SaveAsync(c1);

            return true;
        }

        /// <summary>
        /// Upload the address proof
        /// </summary>
        /// <param name="s">The s</param>
        /// <param name="b">The b</param>
        /// <returns>The task of bool</returns>
        public async Task<bool> UploadAddressProof(string s, byte[] b)
        {
            var c = await this.c.GetAsync(s);
            if (!p2.Validates(c, out var s2, b))
                return false;

            await d.SaveAsync(DocumentType.AddressProof, s, b);

            c.Address = s2;
            await this.c.SaveAsync(c);

            return true;
        }
    }
}
