﻿using System;
using System.ComponentModel.DataAnnotations;

namespace QuandParie.Api.Contracts
{
    public record OddsChange(
        [Required] Guid Id,
        [Range(1, 100)] decimal Value,
        bool? Outcome);
}
